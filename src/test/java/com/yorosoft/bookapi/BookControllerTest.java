package com.yorosoft.bookapi;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.yorosoft.bookapi.config.BookControllerAPIPaths;
import com.yorosoft.bookapi.controller.BookController;
import com.yorosoft.bookapi.dto.BookRequest;
import com.yorosoft.bookapi.exception.BookNotFoundException;
import com.yorosoft.bookapi.model.Book;
import com.yorosoft.bookapi.service.BookService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

@ExtendWith(SpringExtension.class)
@WebMvcTest(BookController.class)
class BookControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private BookService bookService;

    @Captor
    private ArgumentCaptor<BookRequest> argumentCaptor;

    @Test
    void postingANewBookShouldCreateNewBookInTheDatabase() throws Exception {

        BookRequest bookRequest = new BookRequest();
        bookRequest.setAuthor("Duke");
        bookRequest.setTitle("Java 11");
        bookRequest.setIsbn("1337");

        when(bookService.createNewBook(argumentCaptor.capture())).thenReturn(1L);

        this.mockMvc
                .perform(post(BookControllerAPIPaths.BASE_PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(bookRequest)))
                .andExpect(status().isCreated())
                .andExpect(header().exists("Location"))
                .andExpect(header().string("Location", "http://localhost".concat(BookControllerAPIPaths.BASE_PATH).concat("/1")));

        assertThat(argumentCaptor.getValue().getAuthor(), is("Duke"));
        assertThat(argumentCaptor.getValue().getIsbn(), is("1337"));
        assertThat(argumentCaptor.getValue().getTitle(), is("Java 11"));
    }

    @Test
    public void allBooksEndpointShouldReturnTwoBooks() throws Exception {

        when(bookService.getAllBooks()).thenReturn(List.of(
                createBook(1L,"Java 11", "1337"),
                createBook(2L,"Java 8", "42")
        ));

        this.mockMvc
                .perform(get(BookControllerAPIPaths.BASE_PATH))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].title", is("Java 11")))
                .andExpect(jsonPath("$[0].author", is("Duke")))
                .andExpect(jsonPath("$[0].isbn", is("1337")))
                .andExpect(jsonPath("$[0].id", is(1)));
    }

    @Test
    public void getBookWithIdOneShouldReturnABook() throws Exception {

        when(bookService.getBookById(1L)).thenReturn(createBook(1L,"Java 11", "1337"));

        this.mockMvc
                .perform(get(BookControllerAPIPaths.BASE_PATH.concat("/1")))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$.title", is("Java 11")))
                .andExpect(jsonPath("$.author", is("Duke")))
                .andExpect(jsonPath("$.isbn", is("1337")))
                .andExpect(jsonPath("$.id", is(1)));
    }

    @Test
    public void getBookWithIdUnkNowShouldReturn404() throws Exception {

        when(bookService.getBookById(42L)).thenThrow((new BookNotFoundException("Book with id 42 not found")));

        this.mockMvc
                .perform(get(BookControllerAPIPaths.BASE_PATH.concat("/42")))
                .andExpect(status().isNotFound());
    }

    private Book createBook(Long id, String title, String isbn) {

        Book book = new Book();
        book.setId(id);
        book.setTitle(title);
        book.setAuthor("Duke");
        book.setIsbn(isbn);

        return book;
    }
}
