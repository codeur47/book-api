package com.yorosoft.bookapi.service;

import com.yorosoft.bookapi.dto.BookRequest;
import com.yorosoft.bookapi.exception.BookNotFoundException;
import com.yorosoft.bookapi.model.Book;
import com.yorosoft.bookapi.repository.BookRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class BookService {

    private final BookRepository bookRepository;

    public Long createNewBook(BookRequest bookRequest) {
        var book = new Book();
        book.setAuthor(bookRequest.getAuthor());
        bookRequest.setIsbn(bookRequest.getIsbn());
        bookRequest.setTitle(bookRequest.getTitle());

        book = bookRepository.save(book);
        return book.getId();
    }

    public List<Book> getAllBooks(){
        return bookRepository.findAll();
    }

    public Book getBookById(Long id){
        Optional<Book> requestBook = bookRepository.findById(id);
        if (requestBook.isEmpty())
            throw new BookNotFoundException(String.format("Book with id: '%s' not found", id));

        return requestBook.get();
    }
}
