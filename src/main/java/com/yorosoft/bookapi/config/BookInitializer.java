package com.yorosoft.bookapi.config;

import com.github.javafaker.Faker;
import com.yorosoft.bookapi.model.Book;
import com.yorosoft.bookapi.repository.BookRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Slf4j
@AllArgsConstructor
@Component
public class BookInitializer implements CommandLineRunner {

    private final BookRepository bookRepository;

    @Override
    public void run(String... args) {

        try {
            log.info("Starting to initialize sample data....");
            var faker = new Faker();
            for (var i = 0; i < 20; i++) {
                var book = new Book();
                book.setAuthor(faker.book().author());
                book.setTitle(faker.book().title());
                book.setIsbn(UUID.randomUUID().toString());

                bookRepository.save(book);
            }
        }catch (Exception e) {
            log.error("Exception during initialize sample data "+ e);
        }
        finally {
            log.info("Finished to initialize sample data....");
        }
    }
}
