package com.yorosoft.bookapi.config;

public class BookControllerAPIPaths {

    public static final String BASE_PATH = "/api/books";
    public static final String GET_BOOK_BY_ID = "/{id}";

    private BookControllerAPIPaths(){}
}
