package com.yorosoft.bookapi.controller;

import com.yorosoft.bookapi.config.BookControllerAPIPaths;
import com.yorosoft.bookapi.dto.BookRequest;
import com.yorosoft.bookapi.model.Book;
import com.yorosoft.bookapi.service.BookService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping(BookControllerAPIPaths.BASE_PATH)
public class BookController {

    private final BookService bookService;

    @PostMapping
    public ResponseEntity<Void> createNewBook(@RequestBody BookRequest bookRequest, UriComponentsBuilder uriComponentsBuilder) {
        Long id = bookService.createNewBook(bookRequest);
        var uriComponents = uriComponentsBuilder.path(BookControllerAPIPaths.BASE_PATH.concat("/{id}")).buildAndExpand(id);
        var headers = new HttpHeaders();
        headers.setLocation(uriComponents.toUri());

        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<List<Book>> getAllBooks(){
        return ResponseEntity.ok(bookService.getAllBooks());
    }

    @GetMapping(BookControllerAPIPaths.GET_BOOK_BY_ID)
    public ResponseEntity<Book> getBookById(@PathVariable("id") Long id){
        return ResponseEntity.ok(bookService.getBookById(id));
    }

}
